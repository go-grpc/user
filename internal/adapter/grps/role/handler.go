package role

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"user/internal/adapter/grps/marshal"
	"user/internal/adapter/grps/proto"
	"user/internal/domain/role"
	"user/internal/domain/role/usecase"
)

func NewRoleServerGRPC(gServer *grpc.Server, us usecase.IRoleUseCase) {
	server := server{us}
	proto.RegisterRoleServiceServer(gServer, &server)
}

type server struct {
	roleUseCase usecase.IRoleUseCase
}

func (s server) CreateRole(ctx context.Context, request *proto.CreateRoleRequest) (*proto.CreateRoleResponse, error) {
	dto := role.CreateRoleDto{
		Value:       request.Value,
		Description: request.Description,
	}
	id, err := s.roleUseCase.CreateRole(ctx, dto)
	if err != nil {
		return nil, err
	}
	return &proto.CreateRoleResponse{Id: int64(id)}, nil
}

func (s server) GetRoles(ctx context.Context, request *proto.GetRolesRequest) (*proto.GetRolesResponse, error) {
	roles, err := s.roleUseCase.Get(ctx)
	if err != nil {
		return nil, err
	}
	fmt.Println(roles)
	resp := &proto.GetRolesResponse{}
	for _, u := range roles {
		fmt.Print(u)
		resp.Roles = append(resp.Roles, marshal.MarshalRole(u))
	}
	return &proto.GetRolesResponse{Roles: resp.Roles}, nil
}
