package marshal

import (
	"user/internal/adapter/grps/proto"
	"user/internal/domain/entity"
)

func MarshalRole(u entity.Role) *proto.Roles {
	return &proto.Roles{Id: u.Id, Description: u.Description,Value: u.Value}
}
