package user

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"user/internal/adapter/grps/marshal"
	"user/internal/adapter/grps/proto"
	"user/internal/domain/user"
	"user/internal/domain/user/usecase"
)

func NewUserServerGRPC(gServer *grpc.Server, us usecase.IUseCase) {
	server := server{us}
	proto.RegisterUserServiceServer(gServer, &server)
}

type server struct {
	u usecase.IUseCase
}

func (s server) AddRoleToUser(ctx context.Context, request *proto.AddRoleAndRemoveRequest) (*proto.Empty, error) {
	err := s.u.AddRoleToUser(ctx, request.Role, int(request.UserId))
	if err !=nil{
		return nil, err
	}
	return nil, nil
}

func (s server) RemoveRoleToUser(ctx context.Context, request *proto.AddRoleAndRemoveRequest) (*proto.Empty, error) {
	err := s.u.RemoveRoleToUser(ctx,request.Role,int(request.UserId))
	if err !=nil{
		return nil, err
	}
	return nil, nil
}

func (s server) GetUserById(ctx context.Context, request *proto.GetUserByIdRequest) (*proto.GetUserByIdResponse, error) {
	fullUser, err := s.u.GetFullUser(ctx, fmt.Sprintf("%d", request.UserId))
	fmt.Println(fullUser)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	reps := &proto.GetUserByIdResponse{}
	for _, e := range fullUser.Roles {
		reps.Roles = append(reps.Roles, marshal.MarshalRole(e))
	}
	fmt.Println(reps.Roles)
	return &proto.GetUserByIdResponse{
		UserId: int64(fullUser.Id),
		Roles:  reps.Roles,
	}, nil
}

func (s server) CreateUser(ctx context.Context, request *proto.CreateUserRequest) (*proto.CreateUserResponse, error) {
	dto := user.CreateUserDto{
		Phone:    request.GetPhone(),
		Password: request.GetPassword(),
	}
	id, err := s.u.CreateUser(ctx, dto)
	if err != nil {
		return nil, err
	}
	return &proto.CreateUserResponse{Id: int64(id)}, err
}

func (s server) GetUser(ctx context.Context, request *proto.CreateUserRequest) (*proto.CreateUserResponse, error) {
	u, err := s.u.GetUserByPhone(ctx, request.GetPhone())
	if err != nil {
		return nil, err
	}
	return &proto.CreateUserResponse{Id: int64(u.Id)}, nil
}

func (s server) GetUserForLogin(ctx context.Context, check *proto.GetUserForCheck) (*proto.LoginUserResponse, error) {
	fmt.Println(check)
	u, err := s.u.GetUserByPhone(ctx, check.GetPhone())
	if err != nil {
		return nil, err
	}
	fmt.Println(u)
	return &proto.LoginUserResponse{
		Id:       int64(u.Id),
		Phone:    u.Phone,
		Password: u.Password,
	}, nil
}
