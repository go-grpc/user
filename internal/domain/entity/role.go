package entity

type Role struct {
	Id int64 `json:"id"`
	Value string `json:"value"`
	Description string `json:"description"`
}
