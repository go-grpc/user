package entity

type User struct {
	Id int `json:"id"`
	Password string `json:"password"`
	Phone string `json:"phone"`
}

type FullUser struct {
	Id int `json:"id"`
	Phone string `json:"phone"`
	Roles  []Role `json:"roles"`
}
type QueryUser struct {
	User FullUser `json:"user"`
}