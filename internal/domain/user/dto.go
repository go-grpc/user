package user

type CreateUserDto struct {
	Phone string `json:"phone"`
	Password string `json:"password"`
}
type CreateUserResponse struct {
	Phone string `json:"phone"`
	Password string `json:"password"`
	Id int `json:"id" db:"id"`

}
