package usecase

import (
	"context"
	"user/internal/domain/entity"
	"user/internal/domain/user"
)

type IUseCase interface {
	CreateUser(ctx context.Context,dto user.CreateUserDto) (int,error)
    GetUserByPhone(ctx context.Context,phone string) (user.CreateUserResponse,error)
	GetFullUser(ctx context.Context,id string) (entity.FullUser,error)
	AddRoleToUser(ctx context.Context,role string,userId int) error
	RemoveRoleToUser(ctx context.Context,role string,userId int) error
}