package usecase

import (
	"context"
	"errors"
	"fmt"
	"user/internal/domain/entity"
	"user/internal/domain/role/usecase"
	"user/internal/domain/user"
	"user/internal/domain/user/repository"
)

type UseCase struct {
	r           repository.IRepository
	roleUseCase usecase.IRoleUseCase
}

func (u UseCase) RemoveRoleToUser(ctx context.Context, role string, userId int) error {
	roleId, err := u.roleUseCase.GetOneByRole(ctx, role)
	if err != nil {
		return err
	}
	if roleId == 0 {
		return errors.New("такой роль не найден")
	}
	err = u.r.RemoveRoleToUser(ctx, userId, roleId)
	if err != nil {
		return err
	}
	return nil

}

func (u UseCase) AddRoleToUser(ctx context.Context, r string, userId int) error {
	role, err := u.roleUseCase.GetOneByRole(ctx, r)
	fmt.Println(role)
	if err != nil {
		return err
	}
	if role == 0 {
		return errors.New("такой роль не найден")
	}
	err = u.r.AddRoleToUser(ctx, userId, role)
	if err != nil {
		return err
	}
	return nil
}

func (u UseCase) CreateUser(ctx context.Context, dto user.CreateUserDto) (int, error) {
	id, err := u.r.CreateUser(ctx, dto)
	if err != nil {
		return 0, err
	}
	err = u.AddRoleToUser(ctx, "USER", id)
	if err != nil {
          return 0, err
	}
	return id, nil
}

func (u UseCase) GetUserByPhone(ctx context.Context, phone string) (user.CreateUserResponse, error) {
	ur, err := u.r.FindUserByPhone(ctx, phone)
	if err != nil {
		return user.CreateUserResponse{}, err
	}
	fmt.Println(ur)
	return ur, nil

}

func (u UseCase) GetFullUser(ctx context.Context, id string) (entity.FullUser, error) {
	fullUser, err := u.r.FindUserById(ctx, id)
	if err != nil {
		return entity.FullUser{}, err
	}
	return fullUser, nil
}

func NewUseCase(r repository.IRepository, roleUseCase usecase.IRoleUseCase) IUseCase {
	return UseCase{r, roleUseCase}
}
