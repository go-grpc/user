package repository

import (
	"context"
	"fmt"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"user/internal/domain/entity"
	"user/internal/domain/user"
	"user/pkg/client/postgresql"
)

type Repository struct {
	client postgresql.Client
}

func (r Repository) RemoveRoleToUser(ctx context.Context, userId, roleId int) error {
	q := `DELETE FROM user_roles WHERE user_id = $1 AND role_id = $2`
	_, err := r.client.Query(ctx, q, userId, roleId)
	if err != nil {
		return err
	}
	return nil
}

func (r Repository) AddRoleToUser(ctx context.Context, userId, RoleId int) error {
	q := `INSERT INTO user_roles (role_id, user_id) VALUES ($1,$2)`
	_, err := r.client.Query(ctx, q, RoleId, userId)
	if err != nil {
		return err
	}
	return nil
}

func (r Repository) CreateUser(ctx context.Context, dto user.CreateUserDto) (int, error) {
	fmt.Println(dto)
	var id int
	q := `INSERT INTO users (phone, password) VALUES ($1,$2) RETURNING id`
	rows, err := r.client.Query(ctx, q, dto.Phone, dto.Password)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, err
}

func (r Repository) FindUsers(ctx context.Context) ([]entity.User, error) {
	var users []entity.User
	q := `SELECT id , phone FROM users`
	rows, err := r.client.Query(ctx, q)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		if err = rows.Scan(&users); err != nil {
			return nil, err
		}
	}
	return users, nil
}

func (r Repository) FindUserById(ctx context.Context, id string) (entity.FullUser, error) {

	var u entity.FullUser
	var t entity.QueryUser
	q := `
            SELECT jsonb_build_object(
               'id', u.id,
               'phone',u.phone,
               'roles',jsonb_agg(r)
           ) as "user"
FROM users u
         INNER JOIN user_roles ur on u.id = ur.user_id
         INNER JOIN roles r on ur.role_id = r.id
WHERE u.id = $1
GROUP BY u.id
     `
	 err := pgxscan.Get(ctx,r.client,&t,q,id)
	if err != nil {
		return entity.FullUser{}, err
	}
	u.Roles = t.User.Roles
	u.Phone = t.User.Phone
	u.Id = t.User.Id
	fmt.Println(u.Phone)
	return u, nil

}

func (r Repository) FindUserByPhone(ctx context.Context, phone string) (user.CreateUserResponse, error) {
	var u user.CreateUserResponse
	q := `SELECT id , phone,password FROM users where phone = $1`
	row, err := r.client.Query(ctx, q, phone)
	if err != nil {
		if err == pgx.ErrNoRows {
			return user.CreateUserResponse{}, nil
		}
		return user.CreateUserResponse{}, err
	}
	for row.Next() {
		if err = row.Scan(&u.Id,&u.Phone,&u.Password); err !=nil{
			return user.CreateUserResponse{}, err
		}
	}
	return u, nil
}

func NewRepository(client postgresql.Client) IRepository {
	return Repository{client}
}
