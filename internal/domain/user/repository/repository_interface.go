package repository

import (
	"context"
	"user/internal/domain/entity"
	"user/internal/domain/user"
)

type IRepository interface {
	CreateUser(ctx context.Context,dto user.CreateUserDto) (int,error)
	FindUsers(ctx context.Context) ([]entity.User,error)
	FindUserById(ctx context.Context ,id string) (entity.FullUser,error)
	FindUserByPhone(ctx context.Context,phone string) (user.CreateUserResponse,error)
	AddRoleToUser(ctx context.Context,userRoleId,roleId int) error
	RemoveRoleToUser(ctx context.Context,userRoleId,roleId int) error
}


