package usecase

import (
	"context"
	"errors"
	"user/internal/domain/entity"
	"user/internal/domain/role"
	"user/internal/domain/role/repository"
)

type UseCase struct {
	r repository.IRepository
}

func (u UseCase) CreateRole(ctx context.Context, dto role.CreateRoleDto) (int, error) {
	candidate, err := u.GetOneByRole(ctx, dto.Value)
	if err != nil {
		return 0, err
	}
	if candidate != 0 {
		return candidate, errors.New("already exist such role")
	}
	id, err := u.r.CreateRole(ctx, dto)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (u UseCase) GetOneById(ctx context.Context, id string) (int, error) {
	one, err := u.r.FindOne(ctx, id)
	if err != nil {
		return 0, err
	}
	return one, nil
}

func (u UseCase) Get(ctx context.Context) ([]entity.Role, error) {
	roles, err := u.r.Find(ctx)
	if err != nil {
		return nil, err
	}
	return roles, err
}
func (u UseCase) GetOneByRole(ctx context.Context, value string) (int, error) {
	id, err := u.r.GetRoleByValue(ctx, value)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func NewUseCase(r repository.IRepository) IRoleUseCase {
	return &UseCase{r}
}
