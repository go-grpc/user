package usecase

import (
	"context"
	"user/internal/domain/entity"
	"user/internal/domain/role"
)

type IRoleUseCase interface {
	CreateRole(ctx context.Context, dto role.CreateRoleDto) (int, error)
	GetOneById(ctx context.Context, id string) (int, error)
	GetOneByRole(ctx context.Context, value string) (int, error)
	Get(ctx context.Context) ([]entity.Role, error)
}
