package repository

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"user/internal/domain/entity"
	"user/internal/domain/role"
	"user/pkg/client/postgresql"
)

type Repository struct {
	client postgresql.Client
}

func (r Repository) FindOne(ctx context.Context, roleId string) (int, error) {
	var id int
	q := `SELECT id FROM roles WHERE id = $1`
	rows, err := r.client.Query(ctx, q, roleId)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, nil
}

func (r Repository) CreateRole(ctx context.Context, dto role.CreateRoleDto) (int, error) {
	var id int
	q := `INSERT INTO roles (value, description) VALUES($1,$2) RETURNING id`
	rows, err := r.client.Query(ctx, q, dto.Value, dto.Description)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, nil
}

func (r Repository) Find(ctx context.Context) ([]entity.Role, error) {
	var roles []entity.Role
	q := `SELECT jsonb_agg(r)as roles  From roles r `
	err := pgxscan.Get(ctx,r.client,&roles,q)
	if err != nil {
		return nil, err
	}
	return roles, nil
}
func (r Repository) GetRoleByValue(ctx context.Context, value string) (int, error) {
	var id int
	q := `SELECT id From roles where value = $1`
	rows, err := r.client.Query(ctx, q, value)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, nil

}

func NewRepository(client postgresql.Client) IRepository {
	return &Repository{client: client}
}
