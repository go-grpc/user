package repository

import (
	"context"
	"user/internal/domain/entity"
	"user/internal/domain/role"
)

type IRepository interface {
	CreateRole(ctx context.Context,dto role.CreateRoleDto) (int,error)
	Find(ctx context.Context) ([]entity.Role,error)
	FindOne(ctx context.Context ,id string) (int,error)
	GetRoleByValue(ctx context.Context, value string) (int, error)
}


