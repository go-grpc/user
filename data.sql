CREATE TABLE roles
(
    id          serial PRIMARY KEY,
    value       varchar(55) unique not null,
    description varchar(155)       not null
);

CREATE TABLE user_roles
(
    id      serial PRIMARY KEY,
    role_id int not null,
    user_id int not null,
    CONSTRAINT user_fk FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT role_fk FOREIGN KEY (role_id) REFERENCES roles (id)

);

INSERT INTO roles (value, description)
VALUES ('USER', 'role of website')
RETURNING id;
INSERT INTO users (phone, password)
VALUES ('87075545401', '12345');
INSERT INTO user_roles (role_id, user_id)
VALUES (2, 1);


SELECT u.id,
       u.phone,
       jsonb_agg(r) as roles
FROM users u
         INNER JOIN user_roles ur on u.id = ur.user_id
         INNER JOIN roles r on ur.role_id = r.id
GROUP BY u.id;


SELECT jsonb_agg(r) as roles
From roles r;

SELECT t.token, u.phone
FROM users u

         LEFT JOIN tokens t on u.id = t.user_id;



-- SELECT "profile"."createdAt"   AS "profile_createdAt"
--      , "profile"."updateAt"    AS "profile_updateAt"
--      , "profile"."id"          AS "profile_id"
--      , "profile"."description" AS "profile_description"
--      , "profile
-- "."age"                 AS "profile_age"
--      , "profile"."firstName"   AS "profile_firstName"
--      , "profile"."secondName"  AS "profile_secondName"
--      , "profile"."middleName"  AS "profile_middleName"
--      , "profile"."kids"        AS
--                                   "profile_kids"
--      , "profile"."date"        AS "profile_date"
--      , "profile"."iin"         AS "profile_iin"
--      , "profile"."user_id"     AS "profile_user_id"
--      , "profile"."avatarId"    AS "profile_avatarId"
--      , "profile"."cat
-- egoryId"      AS "profile_categoryId"
--      , "profile"."genderId"    AS "profile_genderId"
--      , "profile"."religionId"  AS "profile_religionId"
--      , "profile"."regionId"    AS "profile_regionId"
--      , "hobbies"."id"          AS
--                                   "hobbies_id"
--      , "hobbies"."value"       AS "hobbies_value"
--      , "hobbies"."genderId"    AS "hobbies_genderId"
--      , "gender"."id"           AS "gender_id"
--      , "gender"."value"        AS "gender_value"
--      , "category"."id"         AS "cate
-- gory_id"
--      , "category"."value"      AS "category_value"
--      , "category"."genderId"   AS "category_genderId"
--      , "religion"."id"         AS "religion_id"
--      , "religion"."value"      AS "religion_value"
--      , "region"."id"           AS
--                                   "region_id"
--      , "region"."value"        AS "region_value"
--      , "avatar"."id"           AS "avatar_id"
--      , "avatar"."image"        AS "avatar_image"
--      , "avatar"."profileId"    AS "avatar_profileId"
--      , "photos"."id"           AS "photos_id"
--      , "photos"."image"        AS "photos_image"
--      , "photos"."profileId"    AS "photos_profileId"
-- FROM "profile" "profile"
--          LEFT JOIN "profile_hobbies__hobby_profiles" "profile_hobbies" ON "profile_hobbies
-- "."profileId" = "profile"."id"
--          LEFT JOIN "hobby" "hobbies" ON "hobbies"."id" = "profile_hobbies"."hobbyId"
--          LEFT JOIN "gender" "gender" ON "gender"."id" = "profile"."genderId"
--          LEFT JOIN "categ
-- ory" "category" ON "category"."id" = "profile"."categoryId"
--          LEFT JOIN "religion" "religion" ON "religion"."id" = "profile"."religionId"
--          LEFT JOIN "region" "region" ON "region"."id" = "profile
-- "."regionId"
--          LEFT JOIN "profile_photos" "avatar" ON "avatar"."id" = "profile"."avatarId"
--          LEFT JOIN "profile_photos" "photos" ON "photos"."profileId" = "profile"."id"
-- WHERE "profile"."id" !=
--       $1
--   AND "profile"."genderId" != $2
--   AND "profile"."age" >= $3
--   AND "profile"."age" <= $4
--   AND "profile"."genderId" != $5
-- LIMIT 10
