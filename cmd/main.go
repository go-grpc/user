package main

import (
	"context"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	"os"
	"user/internal/adapter/grps/role"
	"user/internal/adapter/grps/user"
	"user/internal/domain/role/repository"
	"user/internal/domain/role/usecase"
	repository2 "user/internal/domain/user/repository"
	usecase2 "user/internal/domain/user/usecase"
	"user/pkg/client/postgresql"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}
	client, err := postgresql.NewClient(context.Background(), 5, "postgres", "12345", "localhost", "5432", "my_bus")
	if err != nil {
		panic("helloooo")
	}
	listener, err := net.Listen("tcp", ":"+os.Getenv("PORT"))

	roleRepository := repository.NewRepository(client)
	userRepository := repository2.NewRepository(client)

	roleUseCase := usecase.NewUseCase(roleRepository)
	userUseCase := usecase2.NewUseCase(userRepository, roleUseCase)
	srv := grpc.NewServer()
	role.NewRoleServerGRPC(srv, roleUseCase)
	user.NewUserServerGRPC(srv, userUseCase)
	reflection.Register(srv)
	if err := srv.Serve(listener); err != nil {
		panic(err)
	}
	println("server started")

}
